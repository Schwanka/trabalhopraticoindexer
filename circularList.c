#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct word
{
    char palavra[48];
    int qtd;
};

typedef struct elemento *Lista;

// Defini��o do tipo lista
struct elemento
{
    struct word dados;
    struct elemento *prox;
};
typedef struct elemento Elem;

Lista *cria_lista()
{
    Lista *li = (Lista *)malloc(sizeof(Lista));
    if (li != NULL)
        *li = NULL;
    return li;
}
int tamanho_lista(Lista *li);

void libera_lista(Lista *li)
{
    if (li != NULL && (*li) != NULL)
    {
        Elem *aux, *no = *li;
        while ((*li) != no->prox)
        {
            aux = no;
            no = no->prox;
            free(aux);
        }
        free(no);
        free(li);
    }
}

int consulta_lista_pos(Lista *li, int pos, struct word *al)
{
    if (li == NULL || (*li) == NULL || pos <= 0)
        return 0;
    Elem *no = *li;
    int i = 1;
    while (no->prox != (*li) && i < pos)
    {
        no = no->prox;
        i++;
    }
    if (i != pos)
        return 0;
    else
    {
        al = &no->dados;
        return al->qtd;
    }
}

// int consulta_lista_mat(Lista* li, char *pal){
//     if(li == NULL || (*li) == NULL)
//         return 0;
//     Elem *no = *li;
//     while(no->prox != (*li) && no->dados.palavra != pal)
//         no = no->prox;
//     if(no->dados.palavra != pal)
//         return 0;
//     else{
//         // *al = no->dados;
//         return no->dados.qtd;
//     }
// }
int compare1(char a[], char b[])
{
    int flag = 0, i = 0;                 // integer variables declaration
    while (a[i] != '\0' && b[i] != '\0') // while loop
    {
        if (a[i] != b[i])
        {
            flag = 1;
            break;
        }
        i++;
    }
    if (flag == 0)
        return 0;
    else
        return 1;
}
int consulta_lista_mat(Lista *li, char *pal, struct word *al)
{
    if (li == NULL || (*li) == NULL)
        return 0;
    Elem *no = *li;
    int result = compare1(no->dados.palavra, pal);
    if (tamanho_lista(li) == 1)
    {
        if (result == 0)
        {
            return 1;
        }
    }
    while (no->prox != (*li))
    {
        no = no->prox;
        if (result == 0)
            return 1;
        int result = compare1(no->dados.palavra, pal);
        if (result == 0)
            return 1;
    }
    return 0;
}

int tamanho_lista(Lista *li)
{
    if (li == NULL || (*li) == NULL)
        return 0;
    int cont = 0;
    Elem *no = *li;
    do
    {
        cont++;
        no = no->prox;
    } while (no != (*li));
    return cont;
}

int comparaQuantidade(Lista *li, int qtdArvore)
{
    Elem *aux = *li;
    Elem *no;
    while (aux->prox != (*li))
        aux = aux->prox;
    aux->prox = no;
    no->prox = *li;
    if (no->dados.qtd < qtdArvore)
        return 1;
    return 0;
}
void swap(struct elemento *a, struct elemento *b)
{
    struct word temp = a->dados;
    a->dados = b->dados;
    b->dados = temp;
}
void bubbleSort(Lista *start)
{
    int swapped, i;
    struct elemento *ptr1;
    struct elemento *lptr = NULL;
    /* Checking for empty list */
    if (start == NULL)
        return;
    do
    {
        swapped = 0;
        ptr1 = *start;
        while (ptr1->prox != *start)
        {
            if (ptr1->dados.qtd < ptr1->prox->dados.qtd)
            {
                swap(ptr1, ptr1->prox);
                swapped = 1;
            }
            ptr1 = ptr1->prox;
        };
        lptr = ptr1;
    } while (swapped);
}

int insere_lista_final(Lista *li, char *al, int tamList)
{
    if (li == NULL)
        return 0;
    Elem *no = (Elem *)malloc(sizeof(Elem));
    if (no == NULL)
        return 0;
    strcpy(no->dados.palavra, al);
    no->dados.qtd = 1;
    if ((*li) == NULL)
    { // lista vazia: insere in�cio
        *li = no;
        no->prox = no;
    }
    else
    {
        Elem *aux = *li;
        int consulta, tam = tamanho_lista(li);
        consulta = consulta_lista_mat(li, al, &no->dados);
        if ((consulta == 0) && tam < tamList)
        {
            while (aux->prox != (*li))
                aux = aux->prox;
            aux->prox = no;
            no->prox = *li;
            bubbleSort(li);
        }
        else if (consulta == 1)
        {
            while (strcmp(aux->dados.palavra, al) != 0)
            {
                aux = aux->prox;
            }
            aux->dados.qtd++;
            bubbleSort(li);
        }
    }
    return 1;
}
int remove_lista_inicio(Lista *li)
{
    if (li == NULL)
        return 0;
    if ((*li) == NULL) // lista vazia
        return 0;

    if ((*li) == (*li)->prox)
    { // lista fica vaza
        free(*li);
        *li = NULL;
        return 1;
    }
    Elem *atual = *li;
    while (atual->prox != (*li)) // procura o �ltimo
        atual = atual->prox;

    Elem *no = *li;
    atual->prox = no->prox;
    *li = no->prox;
    free(no);
    return 1;
}

int remove_lista_final(Lista *li)
{
    if (li == NULL)
        return 0;
    if ((*li) == NULL) // lista vazia
        return 0;

    if ((*li) == (*li)->prox)
    { // lista fica vaza
        free(*li);
        *li = NULL;
        return 1;
    }
    Elem *ant, *no = *li;
    while (no->prox != (*li))
    { // procura o �ltimo
        ant = no;
        no = no->prox;
    }
    ant->prox = no->prox;
    free(no);
    return 1;
}

int remove_lista(Lista *li, char *mat)
{
    if (li == NULL)
        return 0;
    if ((*li) == NULL) // lista vazia
        return 0;
    Elem *no = *li;
    if (no->dados.palavra == mat)
    { // remover do in�cio
        if (no == no->prox)
        { // lista fica vaza
            free(no);
            *li = NULL;
            return 1;
        }
        else
        {
            Elem *ult = *li;
            while (ult->prox != (*li)) // procura o �ltimo
                ult = ult->prox;
            ult->prox = (*li)->prox;
            *li = (*li)->prox;
            free(no);
            return 1;
        }
    }
    Elem *ant = no;
    no = no->prox;
    while (no != (*li) && no->dados.palavra != mat)
    {
        ant = no;
        no = no->prox;
    }
    if (no == *li) // n�o encontrado
        return 0;

    ant->prox = no->prox;
    free(no);
    return 1;
}

int lista_cheia(Lista *li)
{
    return 0;
}

int lista_vazia(Lista *li)
{
    if (li == NULL)
        return 1;
    if (*li == NULL)
        return 1;
    return 0;
}

void imprime_lista(Lista *li)
{
    if (li == NULL || (*li) == NULL)
        return;
    Elem *no = *li;
    do
    {
        printf("\nPalavra: %s\n", no->dados.palavra);
        printf("Qtd de vezes repetidas: %i\n", no->dados.qtd);
        printf("-------------------------------\n");
        no = no->prox;
    } while (no != (*li));
}
/*
int main()
{
    char *a[19] = {"Oi", "Oi", "Ola", "Hello", "Oi", "Hello", "tarde", "triste", "feliz", "chocalho", "carribean", "Oi", "queen", "Easy", "sofro", "tarde", "entender", "feliz", "chocalho"};
    Lista *li = cria_lista();
    int i;
    for (i = 0; i < 19; i++)
    {
        insere_lista_final(li, a[i], 10);
    }
    imprime_lista(li);
    printf("\n\n\n\n Tamanho: %d\n", tamanho_lista(li));
    libera_lista(li);
    system("pause");
    return 0;
}
*/
