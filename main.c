#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "insereArquivoArvore.c"
#include "contaVezesPalavra.c"
#include "contaVezesPalavraTotal.c"
#include "contaPalavraExiste.c"
#include "populaTermo.c"
int main(int var1, char *args[])
{
    if (strcmp(args[1], "--freq") == 0)
    {
        int numPalavras = 0;
        char *arquivo;
        arquivo = args[3];
        numPalavras = atoi(args[2]);
        insereArquivoArvore(numPalavras, arquivo);
    }
    else if (strcmp(args[1], "--freq-word") == 0)
    {
        long quantidade = 0;
        quantidade = contaVezesPalavra(args[3], args[2]);
        printf("\n A palavra: %s apareceu %li vezes.", args[2], quantidade);
    }
    else if (strcmp(args[1], "--search") == 0)
    {
        int i, iterator, numPalavrasFrase, quantidade, numDocTermoPresente;
        float tf, idf, tfidf;
        long quantidadePalavra, total;
        char aux[300][47];
        numPalavrasFrase = 0;
        tf = 0;
        idf = 0;
        tfidf = 0;
        quantidadePalavra = 0;
        total = 0;
        numDocTermoPresente = 0;
        quantidade = var1 - 3; // pega quantos nomes de arquivos foram passados.
        numPalavrasFrase = populaTermo(aux, args[2]);
        for (iterator = 0; iterator < numPalavrasFrase; iterator++)
        {
            tf = 0;
            idf = 0;
            for (i = 0; i < quantidade; i++)
                numDocTermoPresente += contaPalavraExiste(args[3 + i], aux[iterator]);
            if (numDocTermoPresente > 0)
            {
                printf("\n \t\t Palavra %s ",aux[iterator]);
                for (i = 0; i < quantidade; i++)
                {
                    quantidadePalavra = contaVezesPalavraTotal(args[3 + i], aux[iterator], &total);
                    tf = tf + ((float)quantidadePalavra) / ((float)total);
                    tf = (tf / quantidade); // faz a média
                    idf = log(((double)quantidade / (double)numDocTermoPresente));
                    tfidf = tf * idf;
                    printf("\n \t Documento %s\n\t TF: %f \t IDF: %f\t TFIDF : %f\n",args[3+i], tf,idf,tfidf);
                    total = 0;
                }
            }
        }
    }
    else
        return 1;
    return 0;
}
