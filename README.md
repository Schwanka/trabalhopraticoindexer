# Trabalho Pratico Indexer
Para executar o arquivo, primeiro precisa-se abrir o terminal e ir até o diretório onde estão os arquivos.
Dentro do diretório, executa o seguinte comando para compilar o programa:
    g++ main.c -o indexer 
com o programa compilado, basta executalo como nos exemplos:
    ./indexer --freq 10 101.txt
    ./indexer --freq-word the 101.txt
    ./indexer --search "the brown cow" 101.txt 104.txt

Obs. O nome dos arquivos é o caminho para acessá-lo.
