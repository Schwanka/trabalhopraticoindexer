// voidinsereArquivoArvore(arvore* arvoreBin, char[] arquivo, Lista*  listaFinal){
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "circularList.c"
#define N 47

typedef struct arvore
{
    char info[N + 1];
    int qtd;
    struct arvore *esq;
    struct arvore *dir;
} Arvore;

int compare(char a[], char b[])
{
    int flag = 0, i = 0;                 // integer variables declaration
    while (a[i] != '\0' && b[i] != '\0') // while loop
    {
        if (a[i] != b[i])
        {
            flag = 1;
            break;
        }
        i++;
    }
    if (flag == 0)
        return 0;
    else
        return 1;
}

int buscar(Arvore *tree, char *word)
{
    if (tree)
    {
        /* If tree is not NULL... */

        /* recursively process esq subtree if present.. */
        if (tree->esq)
            buscar(tree->esq, word);

        /* then check the current node.. */
        int value = compare(tree->info, word);
        if (!value)
        {
            return tree->qtd;
        }
        else
        {
            return 0;
        }
        /* then recursively process the dir subtree if present. */
        if (tree->dir)
            buscar(tree->dir, word);
    }
    else
    {

        return 0;
    }
    return 0;
}

Arvore *insert(Arvore *node, char *word)
{
    if (node == NULL)
    {
        node = (Arvore *)malloc(sizeof(Arvore));
        strcpy(node->info, word);
        node->esq = NULL;
        node->dir = NULL;
        node->qtd = 1;
    }
    else
    {

        if (buscar(node, word) == 0)
        {
            if (strcmp(word, node->info) < 0)
                node->esq = insert(node->esq, word);
            else if (strcmp(word, node->info) > 0)
                node->dir = insert(node->dir, word);
            return node;
        }
        else
            node->qtd++;
    }
    return node;
}

void deallocate(Arvore *p)
{
    if (p == NULL)
        return;

    deallocate(p->esq);
    deallocate(p->dir);

    free(p);
}
void in_order(Arvore *a)
{
    if (!a)
        return;
    in_order(a->esq);
    printf("%s %d /", a->info, a->qtd);
    in_order(a->dir);
}
void insereArquivoArvore(int numeroPassado, char arquivo[])
{
    // Inicializa filas e variáveis auxiliares.
    FILE *arquivotxt;
    int c, i;
    c = 0;
    i = c;
    char stringChar[47];
    Arvore *arvoreBin;
    Lista *li = cria_lista();
//    arvoreBin = insert(NULL, "iniciou");

    /*https://www.google.com/amp/s/www.bbc.com/portuguese/curiosidades-43938059.amp
    Segundo esse  site, a maior palavra da língua portuguesa tem 46 caracteres.
    */
    arquivotxt = fopen(arquivo, "r");
    while (1)
    { // While que lê o arquivo todo e o insere na árvore.
        c = fgetc(arquivotxt);
        if ((c > 64 && c < 91) || (c > 96 && c < 123))
        { // Achando o caracter 'A-Z ou ''a-z'' ' ele insere no vetor.
            if (c > 64 && c < 91)
            {
                c = c + 32;
            }
            stringChar[i] = c;
            i++;
        }
        else
        { // ele pula o caracter.
            if (i > 2)
            { // se tem algo no vetor, ele insere.
                arvoreBin = insert(arvoreBin, stringChar);
                // inserir na lista.
                if (tamanho_lista(li) < numeroPassado)
                {
                    insere_lista_final(li, stringChar, numeroPassado);
//                }
//                else if (comparaQuantidade(li, arvoreBin->qtd) == 1)
//                {
//                    printf("\n Entrou no if");
//                }
                 }
           }
            i = 0; // zera o i.
            memset(stringChar, 0, 47);
        }
        if (feof(arquivotxt))
        { // achando o fim do arquivo ele sai do while.
            printf("\n Arvore: \n");
            in_order(arvoreBin);
            printf("\n Lista:\n");
            imprime_lista(li);
            printf("\n");
            return;
        }

    } // fim do while
    // imprime a lista.
    deallocate(arvoreBin);
}
