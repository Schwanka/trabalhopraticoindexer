int populaTermo(char termo[][47], char termoPassado[])
{
    int tamanhoInicial = strlen(termoPassado);
    int i = 0;
    char *ptr = strtok(termoPassado, " ");
    while (ptr != NULL)
    {
        strcpy(termo[i], ptr);
        ptr = strtok(NULL, " ");
        i++;
    }
    return i;
}